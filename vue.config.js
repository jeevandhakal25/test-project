module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/test/' + process.env.CI_PROJECT_NAME + '/'
    : '/'
}